# lib/player.rb

class HumanPlayer
  attr_reader :name

  # def initialize(name)
  #   @name = name
  # end

  def get_play
    print 'What is your next move?: '
    gets.chomp.split(',').map(&:to_i)
  end

  def display(board)
    board.display
  end
end
