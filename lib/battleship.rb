# lib/battleship.rb
require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.new)
    board.populate_grid
    @player = player
    @board = board
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    attack(@player.get_play)
  end

  def play
    until game_over?
      display_status
      play_turn
    end
  end

  def display_status
    puts "Ships left: #{count}"
    @board.display
  end
end
