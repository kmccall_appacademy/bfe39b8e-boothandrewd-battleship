# lib/board.rb

class Board
  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = self.class.default_grid)
    @grid = grid
  end

  def [](pos)
    @grid.dig(*pos)
  end

  def []=(pos, val)
    @grid[pos[0]][pos[1]] = val
  end

  def display
    puts ' ' + ('-' * ((@grid.first.size * 2) + 1)) + ' '
    @grid.each do |row|
      puts '| ' + row.map { |val| val || '.' }.join(' ') + ' |'
    end
    puts ' ' + ('-' * ((@grid.first.size * 2) + 1)) + ' '
  end

  def count
    @grid.map { |row| row.count { |val| val == :s } }.reduce(:+)
  end

  def populate_grid(quantity = ((@grid.count * @grid.first.count)**0.5).round)
    quantity.times { place_random_ship }
  end

  def in_range?(pos)
    (0...@grid.size) === pos[0] && (0...@grid.first.size) === pos[1]
  end

  def won?
    count.zero?
  end

  def place_random_ship
    raise if full?
    loop do
      x_guess = rand(@grid.size)
      y_guess = rand(@grid.first.size)

      next unless empty?([x_guess, y_guess])
      @grid[x_guess][y_guess] = :s
      break
    end
  end

  def empty?(pos = nil)
    if pos
      !@grid.dig(*pos)
    else
      @grid.all? { |row| row.all?(&:nil?) }
    end
  end

  def full?
    @grid.all? { |row| row.none?(&:nil?) }
  end
end
